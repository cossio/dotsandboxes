require './lib/Player'
describe Player do

    before(:each) do 
        @player=Player.new
        @player.add("Bronzuna","red","X")
    end

    it "obtener el nombre de un jugador" do
        expect(@player.getName).to eq "Bronzuna"
    end
    it "obtener el color de un jugador" do
        expect(@player.getColor).to eq "red"
    end
    it "obtener el caracter de un jugador" do
        expect(@player.getCharacter).to eq "X"
    end
    it "obtener el puntaje inicial de un jugador" do
        expect(@player.getPoints).to eq 0
    end
    it "incrementar en 1 el puntaje de un jugador" do
        @player.increasePoints
        expect(@player.getPoints).to eq 1
    end


end