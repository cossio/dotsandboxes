require './lib/Board'
require './lib/Settings'
describe Board do
    before(:each)do
        settings=Settings.new
        settings.inizialitation
        name="Fulanito"#el primer jugador simpre sera X
        settings.addPlayer(name)
        name2="Cosme"#el segundo siempre sera O
        settings.addPlayer(name2)

        @board=Board.new
        width=3
        height=3

        #lo que significa el el tablero tendra 9 cajas
        @board.startGame(width,height,settings)
    end

    it "obtener el turno del jugador" do
        #como todavia no se jugo nada el turno es del primero X
        expect(@board.turn).to eq "X"
    end
    it "generar el tablero" do
        @board.drawBoard     
    end
    it "obtener el valor de la linea de una caja" do
        numberLine=24#es el valor de la linea de abajo de la caja 9, que es la ultima caja del tablero
        @board.drawBoard
        expect(@board.getLineOfTheBox(numberLine)).to eq 24 
    end
    
    it "marcar el valor de la linea de una caja, por el caracter del jugador" do
        numberLine=4#es el valor de la linea de abajo de la caja 9, que es la ultima caja del tablero
        @board.drawBoard
        @board.checkLineOfTheBox(numberLine)
        #como es el primer jugador es X
        expect(@board.getLineOfTheBox(4)).to eq 0..2 #no pude hacer pasar este, revise y nada
    end
    it "obtener el valor de la linea de una caja" do
        expect(@board.changeTurn).to eq "O" #el primero era X
    end

    it "obtener el contenido de una caja, es decir si esta tiene el caracter de algun jugador" do
        @board.drawBoard
        #aqui solo generamos el tablero no registramos ninguna jugada por tanto el contenido de todas las cajas es ""
        expect(@board.getContent(0,0)).to eq "" #de la primera caja [0][0]
    end
    
    it "calcular el puntaje de los jugadores" do
        @board.drawBoard
        @board.calculatePoints
        #como no se hizo ninguna jugada, por tanto tampoco se lleno una caja, el puntaje es 0
        #este ya no supe como hacer
        
    end
end