require './lib/Line'
describe Line do
    before(:each)do
        @line=Line.new
        @line.initialization(4)
    end

    it "obtener el valor de una linea" do
        expect(@line.getValue).to eq 4
    end
    it "obtener el id de una linea" do
        expect(@line.getId).to eq 4
    end
    it "guardar el nuevo valor de una linea" do
        characterPlayer="X"
        @line.setValue(characterPlayer)
        expect(@line.getValue).to eq "X"
    end
end