require './lib/Settings'

describe Settings do
    before(:each)do
        @settings=Settings.new
        @settings.inizialitation
    end

    it "obtener el numbero de jugadores añadidos" do
        expect(@settings.getNumberPlayers).to eq 0
    end

    it "añadir un jugador" do
        name="Fulanito"
        @settings.addPlayer(name)
        expect(@settings.getNumberPlayers).to eq 1
    end
    
end