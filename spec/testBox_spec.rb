require './lib/Box'
require './lib/Player'

describe Box do
    before(:each) do
        @box=Box.new
         #valores de las 4 lineas que forman las caja
        topLine=1
        leftLine=2
        rightLine=3
        bottomLine=4
        @box.initialization(topLine,leftLine,rightLine,bottomLine)
        @player=Player.new
        @player.add("Bronzuna","red","X")
    end    

    it "obtener el estado de la caja llena, es decir si ya se sumo al puntaje de algun jugador" do
        expect(@box.getAddToPointsPlayer).to eq false
    end
    it "cambiar el estado de la caja llena, es decir si ya se sumo al puntaje de algun jugador" do
        
        @box.addToPointsPlayer
        expect(@box.getAddToPointsPlayer).to eq true
    end
    it "retornar la cantidad de lineas ocupadas" do
        expect(@box.getBusyLines).to eq 0
    end
    
    it "guardar la jugada que hizo un jugador, dependiendo de la linea que eligio presionar" do
        move=3# movida ,lo que significa que se eligio la linea derecha de la caja
        @box.saveMove(move,@player.getCharacter)
        #las lineas ocupadas deberian ser 1
        expect(@box.getBusyLines).to eq 1
    end

    it "verificar si se tiene ese numero de linea" do 
        numberLine=4
        expect(@box.isHere(numberLine)).to eq true
    end
    it "obtener el valor actual que tiene la linea" do 
        #la linea puede tener el caracter el jugador X.... o seguir con el numero que se le asigno
        numberLine=4
        expect(@box.getLine(numberLine)).to eq 4
    end

    it "marcar la caja con el caracter del jugador que la cerro" do

        move=1
        @box.saveMove(move,@player.getCharacter)
        move=2
        @box.saveMove(move,@player.getCharacter)
        move=3
        @box.saveMove(move,@player.getCharacter)
        move=4
        @box.saveMove(move,@player.getCharacter)
        @box.paintBox(@player.getCharacter)
        expect(@box.getContent).to eq "X"
    end
    it "obtener el contenido de la caja,es decir a que jugador(caracter) le pertenece" do 
        empty=""
        expect(@box.getContent).to eq empty
    end
end