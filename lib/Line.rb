class Line
    def initialization(value)
        @id=value
        @value=value
    end

    def setValue(value)
        @value=value
    end

    def getValue
        return @value
    end

    def getId
        return @id
    end

end