require './lib/Line'
class Box
    
    def initialization(topLine,leftLine,rightLine,bottonLine)
        @topLine=Line.new
        @topLine.initialization(topLine)

        @leftLine=Line.new
        @leftLine.initialization(leftLine)

        @rightLine=Line.new
        @rightLine.initialization(rightLine)
        
        @bottonLine=Line.new
        @bottonLine.initialization(bottonLine)
        @busyLines=0
        @content=""
        @addToPointsPlayer=false
    end

    def getAddToPointsPlayer
        return @addToPointsPlayer
    end

    def addToPointsPlayer
        @addToPointsPlayer=true
    end

    def saveMove(move,characterPlayer)
        top=@topLine.getId
        left=@leftLine.getId
        right=@rightLine.getId
        bottom=@bottonLine.getId
        
        case move
            when top
                @topLine.setValue(characterPlayer)
                @busyLines+=1
            when left
                @leftLine.setValue(characterPlayer)
                @busyLines+=1            
            when right
                @rightLine.setValue(characterPlayer)
                @busyLines+=1            
            when bottom
                @bottonLine.setValue(characterPlayer)
                @busyLines+=1
        end
    end

    def isHere(numberLine)
        top=@topLine.getValue
        left=@leftLine.getValue
        right=@rightLine.getValue
        bottom=@bottonLine.getValue
        

        ishere=false
        case numberLine
        when top
            ishere=true
        when left
            ishere=true        
        when right
            ishere=true        
        when bottom
            ishere=true
        end
        return ishere
    end

    def getLine(numberLine)
        top=@topLine.getId
        left=@leftLine.getId
        right=@rightLine.getId
        bottom=@bottonLine.getId
        
        case numberLine
        when top
            return @topLine.getValue
        when left
            return @leftLine.getValue
        when right
            return @rightLine.getValue
        when bottom
            return @bottonLine.getValue
        end
        
    end   


    def paintBox(characterPlayer)
        if(@busyLines==4)
            @content=characterPlayer
            @busyLines+=1
        end
    end

    def getContent
        return @content
    end
    
    def getBusyLines
        return @busyLines
    end
end