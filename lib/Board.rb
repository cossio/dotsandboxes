require'./lib/Box'
require'./lib/Player'
require'./lib/Settings'
class Board
    def startGame(width,height,settings)
        
        @width = width
        @height = height
        @boxes=Array.new(@height){Array.new(@width)}
        @players=Array.new(4)
        @settings=settings
        @players=@settings.getPlayers
        @player="X"
        
    end

    def turn
        return @player
    end
    def drawBoard
        top=1
        left=@width+1
        right=left+1
        bottom=right+@width
        for row in(0..(@height-1))
            for col in(0..(@width-1))
                @boxes[row][col]=Box.new
                @boxes[row][col].initialization(top,left,right,bottom)
                top=top+1
                left=left+1
                right=right+1
                bottom=bottom+1
            end
            top=bottom-@width
            left=top+@width
            right=left+1
            bottom=right+@width
        end
    end

    def getLineOfTheBox(numberLine)
        for row in(0..(@height-1))
            for col in(0..(@width-1))
                if(@boxes[row][col].isHere(numberLine)==true)
                    return @boxes[row][col].getLine(numberLine)
                end
            end
        end
    end
    
    def checkLineOfTheBox(numberLine)
        for row in(0..(@height-1))
            for col in(0..(@width-1))
                if(@boxes[row][col].isHere(numberLine)==true)
                    @boxes[row][col].saveMove(numberLine,@player)
                end
            end
        end
    end

    def changeTurn
        if (@player=="X")
            @player="O"
        else
            @player="X"
        end
    end

    def calculatePoints
        for row in(0..(@height-1))
            for col in(0..(@width-1))
                if(@boxes[row][col].getContent=="X" && @boxes[row][col].getAddToPointsPlayer==false)
                    @players[0].increasePoints
                    @boxes[row][col].addToPointsPlayer
                    @player="O"
                end
                if(@boxes[row][col].getContent=="O" && @boxes[row][col].getAddToPointsPlayer==false)
                    @players[1].increasePoints
                    @boxes[row][col].addToPointsPlayer
                    @player="X"
                end
            end
        end
    end

    def paintBoxes        
        for row in(0..(@height-1))
            for col in(0..(@width-1))
                @boxes[row][col].paintBox(@player)
            end
        end

    end

    def getContent(row,col)
        
        return @boxes[row][col].getContent
    
    end
end