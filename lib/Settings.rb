require './lib/Player'
class Settings
    def inizialitation
        @players=Array.new(4)
        @numberPlayers=0
        @colors=["#54ba9b","#ff637d","#5473ba","#ba54a6"]
        @characters=["X","O"]
        #[verde,rojo,azul,lila]  
    end

    def addPlayer(name)
        color=@colors[@numberPlayers]
        character=@characters[@numberPlayers]
        @players[@numberPlayers]=Player.new
        @players[@numberPlayers].add(name,color,character)
        @numberPlayers+=1
    end
    def getNumberPlayers
        return @numberPlayers
    end

    def getPlayers 
        return @players
    end
end