require 'sinatra'
require './lib/Settings'
require './lib/Board'
require './lib/Player'


class App < Sinatra::Base
    $game=Board.new
    $settings=Settings.new
    $settings.inizialitation
    $players=Array.new(4)
    $numberPlayers=0
    $turn=""
    get '/' do
        erb :bienvenida
    end
    get '/principal' do
        erb :principal
    end
    get '/jugarConLaPc' do
        erb :jugarConLaPc
    end
    get '/jugarConAmigos' do
        $game.startGame(3,3,$settings)
        $game.drawBoard
        $turn=$game.turn
        $value1=$game.getLineOfTheBox(1)
        $value2=$game.getLineOfTheBox(2)
        $value3=$game.getLineOfTheBox(3)
        $value4=$game.getLineOfTheBox(4)
        $value5=$game.getLineOfTheBox(5)
        $value6=$game.getLineOfTheBox(6)
        $value7=$game.getLineOfTheBox(7)
        $value8=$game.getLineOfTheBox(8)
        $value9=$game.getLineOfTheBox(9)
        $value10=$game.getLineOfTheBox(10)
        $value11=$game.getLineOfTheBox(11)
        $value12=$game.getLineOfTheBox(12)
        $value13=$game.getLineOfTheBox(13)
        $value14=$game.getLineOfTheBox(14)
        $value15=$game.getLineOfTheBox(15)
        $value16=$game.getLineOfTheBox(16)
        $value17=$game.getLineOfTheBox(17)
        $value18=$game.getLineOfTheBox(18)
        $value19=$game.getLineOfTheBox(19)
        $value20=$game.getLineOfTheBox(20)
        $value21=$game.getLineOfTheBox(21)
        $value22=$game.getLineOfTheBox(22)
        $value23=$game.getLineOfTheBox(23)
        $value24=$game.getLineOfTheBox(24)
        
        $content1=$game.getContent(0,0)
        $content2=$game.getContent(0,1)
        $content3=$game.getContent(0,2)
        $content4=$game.getContent(1,0)
        $content5=$game.getContent(1,1)
        $content6=$game.getContent(1,2)
        $content7=$game.getContent(2,0)
        $content8=$game.getContent(2,1)
        $content9=$game.getContent(2,2)
        erb :jugarConAmigos
    end

    get '/ajustes' do
        erb :ajustes
    end

    post '/ajustes' do
        if($numberPlayers<2)
            $numberPlayers+=1
            $settings.addPlayer(params[:nombre])       
        end
        $players=$settings.getPlayers
        erb :ajustes
    end
    post '/check1' do 
        
        $game.checkLineOfTheBox(1)
        $game.paintBoxes
        $game.calculatePoints
        $content1=$game.getContent(0,0)
        $game.changeTurn
        $turn=$game.turn
        $value1=$game.getLineOfTheBox(1)
        erb :jugarConAmigos
    end
    
    post '/check2' do 
        
        $game.checkLineOfTheBox(2)
        $game.paintBoxes
        $game.calculatePoints
        $content2=$game.getContent(0,1)
        $game.changeTurn
        $turn=$game.turn
        $value2=$game.getLineOfTheBox(2)
        erb :jugarConAmigos
    end
    
    post '/check3' do 
        
        $game.checkLineOfTheBox(3)
        $game.paintBoxes
        $game.calculatePoints
        $content3=$game.getContent(0,2)
        $game.changeTurn
        $turn=$game.turn  
        $value3=$game.getLineOfTheBox(3)
        erb :jugarConAmigos
    end
    
    post '/check4' do 
        
        $game.checkLineOfTheBox(4)
        $game.paintBoxes
        $game.calculatePoints
        $content1=$game.getContent(0,0)
        $game.changeTurn
        $turn=$game.turn
        $value4=$game.getLineOfTheBox(4)
        erb :jugarConAmigos
    end
    
    post '/check5' do 
        
        $game.checkLineOfTheBox(5)
        $game.checkLineOfTheBox(5)
        $game.paintBoxes
        $game.calculatePoints
        $content1=$game.getContent(0,0)
        $content2=$game.getContent(0,1)
        $game.changeTurn
        $turn=$game.turn
        $value5=$game.getLineOfTheBox(5)
        erb :jugarConAmigos
    end
    
    post '/check6' do 
        
        $game.checkLineOfTheBox(6)
        $game.checkLineOfTheBox(6)
        $game.paintBoxes
        $game.calculatePoints
        $content2=$game.getContent(0,1)
        $content3=$game.getContent(0,2)
        $game.changeTurn
        $turn=$game.turn
        $value6=$game.getLineOfTheBox(6)
        erb :jugarConAmigos
    end
    
    post '/check7' do 
        
        $game.checkLineOfTheBox(7)
        $game.paintBoxes
        $game.calculatePoints
        $content3=$game.getContent(0,2)
        $game.changeTurn
        $turn=$game.turn
        $value7=$game.getLineOfTheBox(7)
        erb :jugarConAmigos
    end
    
    post '/check8' do 
        
        $game.checkLineOfTheBox(8)
        $game.checkLineOfTheBox(8)
        $game.paintBoxes
        $game.calculatePoints
        $content1=$game.getContent(0,0)
        $content4=$game.getContent(1,0)
        $game.changeTurn
        $turn=$game.turn
        $value8=$game.getLineOfTheBox(8)
        erb :jugarConAmigos
    end
    
    post '/check9' do 
        
        $game.checkLineOfTheBox(9)
        $game.checkLineOfTheBox(9)
        $game.paintBoxes
        $game.calculatePoints
        $content2=$game.getContent(0,1)
        $content5=$game.getContent(1,1)
        $game.changeTurn
        $turn=$game.turn
        $value9=$game.getLineOfTheBox(9)
        erb :jugarConAmigos
    end
    
    post '/check10' do 
        
        $game.checkLineOfTheBox(10)
        $game.checkLineOfTheBox(10)
        $game.paintBoxes
        $game.calculatePoints
        $content3=$game.getContent(0,2)
        $content6=$game.getContent(1,2)
        $game.changeTurn
        $turn=$game.turn
        $value10=$game.getLineOfTheBox(10)
        erb :jugarConAmigos
    end
    
    post '/check11' do 
        $game.checkLineOfTheBox(11)
        $game.paintBoxes
        $game.calculatePoints
        $content4=$game.getContent(1,0)
        $game.changeTurn
        $turn=$game.turn
        $value11=$game.getLineOfTheBox(11)
        erb :jugarConAmigos
    end
    
    post '/check12' do 
        $game.checkLineOfTheBox(12)
        $game.checkLineOfTheBox(12)
        $game.paintBoxes
        $game.calculatePoints
        $content4=$game.getContent(1,0)
        $content5=$game.getContent(1,1)
        $game.changeTurn
        $turn=$game.turn
        $value12=$game.getLineOfTheBox(12)
        erb :jugarConAmigos
    end




    post '/check13' do 
        $game.checkLineOfTheBox(13)
        $game.checkLineOfTheBox(13)
        $game.paintBoxes
        $game.calculatePoints
        $content5=$game.getContent(1,1)
        $content6=$game.getContent(1,2)
        $game.changeTurn
        $turn=$game.turn
        $value13=$game.getLineOfTheBox(13)
        erb :jugarConAmigos
    end

    post '/check14' do 
        $game.checkLineOfTheBox(14)
        $game.paintBoxes
        $game.calculatePoints
        $content6=$game.getContent(1,2)
        $game.changeTurn
        $turn=$game.turn
        $value14=$game.getLineOfTheBox(14)
        erb :jugarConAmigos
    end

    post '/check15' do 
        $game.checkLineOfTheBox(15)
        $game.checkLineOfTheBox(15)
        $game.paintBoxes
        $game.calculatePoints
        $content4=$game.getContent(1,0)
        $content7=$game.getContent(2,0)
        $game.changeTurn
        $turn=$game.turn
        $value15=$game.getLineOfTheBox(15)
        erb :jugarConAmigos
    end

    post '/check16' do 
        $game.checkLineOfTheBox(16)
        $game.checkLineOfTheBox(16)
        $game.paintBoxes
        $game.calculatePoints
        $content5=$game.getContent(1,1)
        $content8=$game.getContent(2,1)
        $game.changeTurn
        $turn=$game.turn
        $value16=$game.getLineOfTheBox(16)
        erb :jugarConAmigos
    end

    post '/check17' do 
        $game.checkLineOfTheBox(17)
        $game.checkLineOfTheBox(17)
        $game.paintBoxes
        $game.calculatePoints
        $content6=$game.getContent(1,2)
        $content9=$game.getContent(2,2)
        $game.changeTurn
        $turn=$game.turn
        $value17=$game.getLineOfTheBox(17)
        erb :jugarConAmigos
    end

    post '/check18' do 
        $game.checkLineOfTheBox(18)
        $game.paintBoxes
        $game.calculatePoints
        $content7=$game.getContent(2,0)
        $game.changeTurn
        $turn=$game.turn
        $value18=$game.getLineOfTheBox(18)
        erb :jugarConAmigos
    end

    post '/check19' do 
        $game.checkLineOfTheBox(19)
        $game.checkLineOfTheBox(19)
        $game.paintBoxes
        $game.calculatePoints
        $content7=$game.getContent(2,0)
        $content8=$game.getContent(2,1)
        $game.changeTurn
        $turn=$game.turn
        $value19=$game.getLineOfTheBox(19)
        erb :jugarConAmigos
    end

    post '/check20' do 
        $game.checkLineOfTheBox(20)
        $game.checkLineOfTheBox(20)
        $game.paintBoxes
        $game.calculatePoints
        $content8=$game.getContent(2,1)
        $content9=$game.getContent(2,2)
        $game.changeTurn
        $turn=$game.turn
        $value20=$game.getLineOfTheBox(20)
        erb :jugarConAmigos
    end

    post '/check21' do 
        $game.checkLineOfTheBox(21)
        $game.paintBoxes
        $game.calculatePoints
        $content9=$game.getContent(2,2)
        $game.changeTurn
        $turn=$game.turn
        $value21=$game.getLineOfTheBox(21)
        erb :jugarConAmigos
    end

    post '/check22' do 
        $game.checkLineOfTheBox(22)
        $game.paintBoxes
        $game.calculatePoints
        $content7=$game.getContent(2,0)
        $game.changeTurn
        $turn=$game.turn
        $value22=$game.getLineOfTheBox(22)
        erb :jugarConAmigos
    end

    post '/check23' do 
        $game.checkLineOfTheBox(23)
        $game.paintBoxes
        $game.calculatePoints
        $content8=$game.getContent(2,1)
        $game.changeTurn
        $turn=$game.turn
        $value23=$game.getLineOfTheBox(23)
        erb :jugarConAmigos
    end

    post '/check24' do 
        $game.checkLineOfTheBox(24)
        $game.paintBoxes
        $game.calculatePoints
        $content9=$game.getContent(2,2)
        $game.changeTurn
        $turn=$game.turn
        $value24=$game.getLineOfTheBox(24)
        erb :jugarConAmigos
    end

    run! if app_file == $0
end






