Feature: Jugar con amigos
    Como jugador 
    Quiero jugar con mis amigos
    Para divertirme

    Scenario: ver en pantalla la pagina de Ajustes 
        Given visito la pagina Menu principal y veo sus opciones 
        When presiono el boton "Jugar con amigos" 
        Then deberia ver el titulo "Ajustes" para configurar antes de iniciar una partida

    
    Scenario: ver en pantalla el primer jugador añadido
        Given estoy la pagina Ajustes 
        And presiono el boton de "Añadir jugador" 
        Then puedo ver el formulario con el titulo "Añade un jugador"
        And ingreso el nombre "Bronzuna" para "nombre"
        When presiono el boton de "Guardar"
        Then puedo ver en pantalla el jugador "Bronzuna" que añadi

    Scenario: ver en pantalla el segundo jugador añadido
        Given estoy la pagina Ajustes 
        And presiono el boton de "Añadir jugador" 
        Then puedo ver el formulario con el titulo "Añade un jugador"
        And ingreso el nombre "Madona" para "nombre"
        When presiono el boton de "Guardar"
        Then puedo ver en pantalla el jugador "Madona" que añadi

    Scenario: ver en pantalla el segundo jugador
        Given estoy la pagina Ajustes
        And presiono el boton de "Jugar"
        Then deberia estar en al pagina con el titulo "Jugar con amigos"
        And deberia ver en pantalla los nombres "Bronzuna" y "Madona"
        And deberia ver en pantalla los puntajes "0" y "0"
        And deberia ver en pantalla "Turno de :"
        And deberia ver en pantalla el tablero "" 

