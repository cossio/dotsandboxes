Given("visito la pagina Menu principal y veo sus opciones") do
    visit('/principal')
end

When("presiono el boton {string}") do |boton|
    click_link(boton)
end
  
Then("deberia ver el titulo {string} para configurar antes de iniciar una partida") do |titulo|
    expect(page).to have_content(titulo)
end

#DADO QUE ESTOS PASOS DEPENDE DE AÑADIR JUGADOR(PROBLEMA) NO PUDE IMPLEMENTARLOS
#Nota: el problema es que el formulario de añadir un jugador se encuentra no visible el capybara no lo detecta
#solo se pone visible si se presiona el boton de añadir jugador pero aun asi sigue el problema
Then("puedo ver en pantalla el jugador {string} que añadi") do |string|
    pending # Write code here that turns the phrase above into concrete actions
  end

  
  Given("estoy la pagina Ajustes") do
    pending # Write code here that turns the phrase above into concrete actions
  end
  
  Then("deberia estar en al pagina con el titulo {string}") do |string|
    pending # Write code here that turns the phrase above into concrete actions
  end
  
  Then("deberia ver en pantalla los nombres {string} y {string}") do |string, string2|
    pending # Write code here that turns the phrase above into concrete actions
  end
  
  Then("deberia ver en pantalla los puntajes {string} y {string}") do |string, string2|
    pending # Write code here that turns the phrase above into concrete actions
  end
  
  Then("deberia ver en pantalla {string}") do |string|
    pending # Write code here that turns the phrase above into concrete actions
  end
  
  Then("deberia ver en pantalla el tablero {string}") do |string|
    pending # Write code here that turns the phrase above into concrete actions
  end