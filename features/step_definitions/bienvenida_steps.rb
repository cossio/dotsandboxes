Given("visito la pagina Bienvenida") do
    visit('/')
end
  
Then("deberia ver el texto de {string}") do |saludo|
    expect(page).to have_content(saludo)
end
  
Then("deberia ver la imagen de {string}") do |imagen|
    expect(page).to have_css(imagen)
end