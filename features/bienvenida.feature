Feature: Bienvenida
    Como jugador 
    Quiero ver un mensaje de bienvenida "DOTS and BOXES"
    Para sentirme a gusto desde el inicio

    Scenario: mensaje de bienvenida
        Given visito la pagina Bienvenida
        Then deberia ver el texto de "Bienvenido!!"
        And deberia ver la imagen de "div.dot"
